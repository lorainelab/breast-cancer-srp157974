## About

This repository contains scripts and analysis results for SRA dataset SRP157974, RNA-Seq data from triple negative breast cancer from FUSCCTNBC project.

## Questions?

Contact Ann loraine aloraine@uncc.edu