#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=6
#PBS -l mem=72gb
#PBS -l walltime=16:00:00
cd $PBS_O_WORKDIR
module load star
# defined by qsub -v: S, F
G=H_sapiens_Dec_2013
O=SRP056969
if [ ! -d $O ]; then
    mkdir $O
fi
if [ ! -d $O/$S ]; then
    mkdir $O/$S
fi

B=$(basename $F _1.fastq.gz)
STAR --genomeDir ${G} --runThreadN 6 --readFilesIn $F ${B}_2.fastq.gz --readFilesCommand gunzip -c --outFileNamePrefix $O/$S/$S --outSAMtype BAM SortedByCoordinate --sjdbGTFfile H_sapiens_Dec_2013.gtf --alignIntronMax 100000 
