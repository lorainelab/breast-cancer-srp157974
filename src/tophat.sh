#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=8
#PBS -l mem=64gb
#PBS -l walltime=16:00:00
cd $PBS_O_WORKDIR
module load bowtie2
module load tophat
G=H_sapiens_Dec_2013
O=SRP056969_tophat
if [ ! -d $O ]; then
    mkdir $O
fi

# S,F defined by qsub -v 
B=$(basename $F _1.fastq.gz)
tophat -p 8 -I 300000 -o $O/$S $G $F ${B}_2.fastq.gz -G H_sapiens_Dec_2013_curatedRefSeq.gtf 

