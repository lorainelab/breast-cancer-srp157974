#!/bin/bash
#PBS -N scale
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=16gb
#PBS -l walltime=10:00:00

# Make coverage graph files
# with y-axis values multipled by scaling factor and rounded
# to 2 decimals places:
#
#  100,000,000 / number of mappings = scaling factor
#
# Rationale:
#
# Present read count as number of reads per 100 million mappings to allow
# rough comparison between coverage graphs derived from different-sized
# BAM alignment files.
# For example, if a position has 50 overlapping reads and there
# were 100,000,000 mappings in the sample, then the coverage graph
# should report y axis value 
#   50 * 100,000,000/100,000,000 = 50 * 1 = 50
# at that position.
#
# Likewise, if a position has 1 overlapping read and there were 50,000,000
# alignments in the sample, then the coverage graph should report
#    1 * 10,000,000/50,000,000 = 1 * 1/2 = 0.5
# at that position.
#
# In this data set, number of mappings (found by tophat) was
# between around 250 and 80 million.
#
# Flaw: this data set was paired end, and many pairs overlap. This means
# positions where pairs overlap have too many counts. Alignments seeming
# to detect rare variants may exhibit bias toward reads with missing mates.

cd $PBS_O_WORKDIR

# S, F passed in from qsub -v option

module load samtools #tabix, bgzip

MOD=100000000
OUT=$S.scaled.bedgraph.gz
if [ ! -s $OUT ]; 
then
    if [ -s $S.count.txt ];
    then 
	N=`cat $S.count.txt`
    else
	N=`samtools view -c $F`
    fi
    scale=`echo "scale=2; $MOD/$N" | bc`
    genomeCoverageBed -ibam $F -split -bg -scale $scale | bgzip > $OUT
    tabix -s 1 -b 2 -e 3 -f -0 $OUT
fi
